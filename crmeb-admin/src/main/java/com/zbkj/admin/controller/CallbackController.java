package com.zbkj.admin.controller;

import com.zbkj.service.service.CallbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 支付回调
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Slf4j
@RestController
@RequestMapping("api/admin/payment/callback")
@Api(tags = "支付回调")
public class CallbackController {

    @Autowired
    private CallbackService callbackService;

    /**
     * 微信支付回调
     */
    @ApiOperation(value = "微信支付回调")
    @RequestMapping(value = "/wechatv2", method = RequestMethod.POST)
    public String weChat(@RequestBody String  request) {
       // {"id":"a9ea8fbf-39fc-58f1-9423-a08d13ff1841","create_time":"2023-08-16T16:06:33+08:00","resource_type":"encrypt-resource","event_type":"TRANSACTION.SUCCESS","summary":"支付成功","resource":{"original_type":"transaction","algorithm":"AEAD_AES_256_GCM","ciphertext":"tGsN9DK5OiKxWfSJ3UrLKS1zSa+60ITJdxsv/bE0XaoqbUsefzC4Zuyxrnfx05/wgbGuLwl41dC+QMYedFJbQnoQ4waWe/YML0sqeg6/b7JGFHOWE5A+5pXndP3rTTW5HXSY+8gINOdv2NabBvGEtiFTgVjy+IXSJ3dFgcenzIiMmlWI08qWS2lt81Of8yQOtK3Eo2fS/cEd74Q20TEjtZjK0IRgT5GjEo1aAk0eIu0aQH/lwXDpDsPeUgUYh/MOK6bkpMr/T9E3OJs1wpdrZ7NtRfyhoUIU1G71FeIlHT7P+VOiBOYeJj+fVmL4OYynqOtpjuvSHbPilj2AI41FfcJQCGFq6u60w62ScrEhhZZryIFK0UJKydwcQSjZmYvxh0iVt4Er2yTC0zJvIyCraj+ZxlwcymtVqdWjlQ6jUC1zX4iAPRt+GDBeWHoX1Ht/iPB3qeUsqmZ1ArwZBiryskYy3d42ggYqk6ZfIK/LL0SGOxWQRWRY31VyCQX+HlR5u4cix7tJRtZ3yh8WQObXdOvRV3b5OIxlRWKi4PuRirQuKUMCU1RprS1Kr+f5GMGlb0LFzdIuHFs1ZuyA244=","associated_data":"transaction","nonce":"wIZlt7SnalrP"}}
//        {
//            "id": "a9ea8fbf-39fc-58f1-9423-a08d13ff1841",
//                "create_time": "2023-08-16T16:06:33+08:00",
//                "resource_type": "encrypt-resource",
//                "event_type": "TRANSACTION.SUCCESS",
//                "summary": "支付成功",
//                "resource": {
//            "original_type": "transaction",
//                    "algorithm": "AEAD_AES_256_GCM",
//                    "ciphertext": "tGsN9DK5OiKxWfSJ3UrLKS1zSa+60ITJdxsv/bE0XaoqbUsefzC4Zuyxrnfx05/wgbGuLwl41dC+QMYedFJbQnoQ4waWe/YML0sqeg6/b7JGFHOWE5A+5pXndP3rTTW5HXSY+8gINOdv2NabBvGEtiFTgVjy+IXSJ3dFgcenzIiMmlWI08qWS2lt81Of8yQOtK3Eo2fS/cEd74Q20TEjtZjK0IRgT5GjEo1aAk0eIu0aQH/lwXDpDsPeUgUYh/MOK6bkpMr/T9E3OJs1wpdrZ7NtRfyhoUIU1G71FeIlHT7P+VOiBOYeJj+fVmL4OYynqOtpjuvSHbPilj2AI41FfcJQCGFq6u60w62ScrEhhZZryIFK0UJKydwcQSjZmYvxh0iVt4Er2yTC0zJvIyCraj+ZxlwcymtVqdWjlQ6jUC1zX4iAPRt+GDBeWHoX1Ht/iPB3qeUsqmZ1ArwZBiryskYy3d42ggYqk6ZfIK/LL0SGOxWQRWRY31VyCQX+HlR5u4cix7tJRtZ3yh8WQObXdOvRV3b5OIxlRWKi4PuRirQuKUMCU1RprS1Kr+f5GMGlb0LFzdIuHFs1ZuyA244=",
//                    "associated_data": "transaction",
//                    "nonce": "wIZlt7SnalrP"
//        }
//        }
        //Service.wxAppPayNotify(wechatpaySerial,wechatpaySignature,wechatpayTimestamp,wechatpayNonce,callback);


      //=====================================
        System.out.println("微信支付回调 request ===> " + request);
        System.out.println(request);
        String response = callbackService.weChat(request);
        System.out.println("微信支付回调 response ===> " + response);
        return response;
    }

    @ApiOperation(value = "微信支付回调")
    @RequestMapping(value = "/wechat", method = RequestMethod.POST)
    public String weChat(@RequestHeader("Wechatpay-Serial") String wechatpaySerial,
                         @RequestHeader("Wechatpay-Signature") String wechatpaySignature,
                         @RequestHeader("Wechatpay-Timestamp") String wechatpayTimestamp,
                         @RequestHeader("Wechatpay-Nonce") String wechatpayNonce,
                         @RequestBody String callback) {
        System.out.println("微信支付回调 request ===> " + callback);
        String response="";
        try {
             response = callbackService.weChatv3(wechatpaySerial,wechatpaySignature,wechatpayTimestamp,wechatpayNonce,callback);
        }catch (Exception e){
           e.printStackTrace();
        }

        System.out.println("微信支付回调 response ===> " + response);
        return response;
    }

    /**
     * 微信退款回调
     */
    @ApiOperation(value = "微信退款回调")
    @RequestMapping(value = "/wechat/refund", method = RequestMethod.POST)
    public String weChatRefund(@RequestBody String request) {
        System.out.println("微信退款回调 request ===> " + request);
        String response = callbackService.weChatRefund(request);
        System.out.println("微信退款回调 response ===> " + response);
        return response;
    }
}



