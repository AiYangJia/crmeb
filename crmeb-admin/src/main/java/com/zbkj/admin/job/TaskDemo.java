package com.zbkj.admin.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.generator.config.INameConvert;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import com.zbkj.common.constants.Constants;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.order.StoreOrder;
import com.zbkj.common.model.order.StoreOrderInfo;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.StoreOrderSendRequest;
import com.zbkj.common.response.OrderDetailResponse;
import com.zbkj.common.response.OrderInfoResponse;
import com.zbkj.common.response.StoreOrderItemResponse;
import com.zbkj.service.dao.StoreOrderDao;
import com.zbkj.service.service.StoreOrderService;
import com.zbkj.service.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class TaskDemo {

    @Autowired
    private StoreOrderService storeOrderService;

    @Autowired
    private UserService userService;


    @XxlJob("Demo")
    public ReturnT<String> myFirstTask(String param) throws Exception {
      //  String param = XxlJobHelper.getJobParam();
        System.out.println("Demo-----------------------------------Demo----》");
      //  XxlJobHelper.log("测试开始");
        //获取全部今日完成的订单数据 //-------------------------------------------------
        //storeOrderService.getOrderStatusNum();
//        getCount(dateLimit, Constants.ORDER_STATUS_COMPLETE, type)
       //storeOrderService.fenHongForUserback();
        //response.setComplete()
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println(param);
      System.out.println("测试完成！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
//        System.out.println();
//        System.out.println();
//        System.out.println();
      //  XxlJobHelper.log("测试开结束");
        System.out.println("Demo-----------------------------------Demo----》结束");
        System.out.println(param);
        return ReturnT.SUCCESS;
    }

    @XxlJob("Demo2")
    public ReturnT<String> myTwoTask(String param) throws Exception {
        //  String param = XxlJobHelper.getJobParam();
        //  XxlJobHelper.log("测试开始");
        System.out.println("Demo2-----------------------------------Demo2----》开始");
        //storeOrderService.getOrderStatusNum();
//        getCount(dateLimit, Constants.ORDER_STATUS_COMPLETE, type)
        userService.getDianweiJiang(1,1);
        //response.setComplete()
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println(param);
        System.out.println("点位奖计算完成！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
//        System.out.println();
//        System.out.println();
//        System.out.println();
        //  XxlJobHelper.log("测试开结束");
        System.out.println("Demo2-----------------------------------Demo2----》结束");
        System.out.println(param);
        return ReturnT.SUCCESS;
    }


    @Resource
    private StoreOrderDao dao;

    @XxlJob("Demo3")
    public ReturnT<String> myThreeTask(String param) throws Exception {
        //  String param = XxlJobHelper.getJobParam();
        //  XxlJobHelper.log("测试开始");

        //storeOrderService.getOrderStatusNum();
//        getCount(dateLimit, Constants.ORDER_STATUS_COMPLETE, type)
      //  List<StoreOrder> orderList = storeOrderService.getUserOrderList(userId, status, pageRequest);

        System.out.println("Demo3-----------------------------------Demo3----》开始");
        QueryWrapper<StoreOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "order_id", "uid", "real_name", "pay_price", "pay_type", "create_time", "status", "refund_status"
                , "refund_reason_wap_img", "refund_reason_wap_explain", "refund_reason_wap", "refund_reason", "refund_reason_time"
                , "is_del", "combination_id", "pink_id", "seckill_id", "bargain_id", "verify_code", "remark", "paid", "is_system_del", "shipping_type", "type", "is_alter_price");

        queryWrapper.eq("status", "notShipped");
        queryWrapper.eq("type", "0");

        List<StoreOrder> orderList = dao.selectList(queryWrapper);
        for (StoreOrder storeOrder : orderList) {
            Integer id=storeOrder.getStoreId();
            StoreOrderSendRequest request=new StoreOrderSendRequest();
            request.setId(storeOrder.getId());
            request.setOrderNo(storeOrder.getOrderId());
            storeOrder.setDeliveryType("fictitious");
            //错误
            storeOrderService.send(request);
            // 执行订单发货方法

       }


        //response.setComplete()
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println(param);
        System.out.println("自动发货 每10分钟查询订单，进行执行方法！！！！！！！！！！！！！！！！！！！！！！！！！！！！");
//        System.out.println();
//        System.out.println();
//        System.out.println();
        //  XxlJobHelper.log("测试开结束");
        System.out.println("Demo3-----------------------------------Demo3----》结束");
        System.out.println(param);
        return ReturnT.SUCCESS;
    }



}
