package com.zbkj.common.utils;


import com.alibaba.fastjson.JSONObject;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import com.zbkj.common.config.V3WxConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.*;
import java.util.Base64;

/**
 *
 * 签名工具类
 * */
@Component
public class V3WxSignUtil {

    protected static final SecureRandom RANDOM = new SecureRandom();

//    /**
//     * 微信调起支付参数
//     * 返回参数如有不理解 请访问微信官方文档
//     * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_2_4.shtml
//     *
//     * @param prepayId         微信下单返回的prepay_id
//     * @param appId            应用ID
//     * @param mch_id           商户号
//     * @param privateKey       私钥
//     * @return 当前调起支付所需的参数
//     * @throws Exception
//     */
//    public static String WxAppPayTuneUp(String prepayId, String appId, String mch_id, String privateKey ) throws Exception {
//        if (StringUtils.isNotBlank(prepayId)) {
//            long timestamp = System.currentTimeMillis() / 1000;
//            String nonceStr = generateNonceStr();
//            //加载签名
//            String packageSign = sign(buildMessage(appId, timestamp, nonceStr, prepayId).getBytes(), privateKey);
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("appId", appId);
//            jsonObject.put("prepayId", prepayId);
//            jsonObject.put("timeStamp", timestamp);
//            jsonObject.put("nonceStr", nonceStr);
//            jsonObject.put("package", "Sign=WXPay");
//            jsonObject.put("signType", "RSA");
//            jsonObject.put("sign", packageSign);
//            jsonObject.put("partnerId", mch_id);
//            return jsonObject.toJSONString();
//        }
//        return "";
//    }

    public static String WxAppPayTuneUp(String prepayId, String appId, String mch_id) throws Exception {
        if (StringUtils.isNotBlank(prepayId)) {
            Long timestamp =System.currentTimeMillis() / 1000;
            String timestr = Long.toString(timestamp);
            String nonceStr = generateNonceStr();
            //加载签名
            String packageSign = sign(buildMessage(appId, timestamp, nonceStr, prepayId).getBytes());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", appId);
            jsonObject.put("prepayId", prepayId);
            jsonObject.put("timeStamp", timestr);
            jsonObject.put("nonceStr", nonceStr);
            jsonObject.put("package", "Sign=WXPay");
            jsonObject.put("signType", "RSA");
            jsonObject.put("sign", packageSign);
            jsonObject.put("partnerId", mch_id);
            return jsonObject.toJSONString();
        }
        return "";
    }

    public static String sign(byte[] message, String privateKey) throws NoSuchAlgorithmException, SignatureException, IOException, InvalidKeyException {
        //签名方式
        Signature sign = Signature.getInstance("SHA256withRSA");
        //私钥
        sign.initSign(PemUtil
                .loadPrivateKey(privateKey));

        sign.update(message);

        return Base64.getEncoder().encodeToString(sign.sign());
    }

    public static String sign(byte[] message) throws NoSuchAlgorithmException, SignatureException, IOException, InvalidKeyException {
        //签名方式
        Signature sign = Signature.getInstance("SHA256withRSA");
        //私钥
        sign.initSign(V3WxConfig.getPrivateKey());

        sign.update(message);

        return Base64.getEncoder().encodeToString(sign.sign());
    }

    //生成随机字符串 微信底层的方法，直接copy出来了
    protected static String generateNonceStr() {
        char[] nonceChars = new char[32];
        for (int index = 0; index < nonceChars.length; ++index) {
            nonceChars[index] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(RANDOM.nextInt("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".length()));
        }
        return new String(nonceChars);
    }

    /**
     * 按照前端签名文档规范进行排序，\n是换行
     *
     * @param appId     appId
     * @param timestamp 时间
     * @param nonceStr  随机字符串
     * @param prepay_id prepay_id
     * @return
     */
    public static String buildMessage(String appId, long timestamp, String nonceStr, String prepay_id) {
        return appId + "\n"
                + timestamp + "\n"
                + nonceStr + "\n"
                + "prepay_id="+prepay_id + "\n";
    }

//    public static String buildMessage(String appId, long timestamp, String nonceStr, String prepay_id) {
//        return appId + "\n"
//                + timestamp + "\n"
//                + nonceStr + "\n"
//                + "prepay_id="+prepay_id + "\n";
//    }

}


