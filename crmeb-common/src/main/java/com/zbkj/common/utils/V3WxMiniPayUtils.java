package com.zbkj.common.utils;

import cn.hutool.core.io.resource.ClassPathResource;
import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.Verifier;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.cert.CertificatesManager;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import com.zbkj.common.config.V3WxConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

@Slf4j
@Component
public class V3WxMiniPayUtils {

    public static CloseableHttpClient httpClient;

    public static Verifier verifier;


    /**
     * 初始化 HttpClient
     * @throws IOException
     */
    public static void initWXPayClient() throws IOException {
        try {
            // 加载商户私钥（privateKey：私钥字符串）
            //私钥
            PrivateKey merchantPrivateKey = V3WxConfig.getPrivateKey();
            //商户序列号
//            X509Certificate certificate = PemUtil.loadCertificate(inpt2);
//            String serialNo = certificate.getSerialNumber().toString(16).toUpperCase();
            String serialNo = V3WxConfig.getSerialNo();

            //merchantId:商户号,serialNo:商户证书序列号
            // 获取证书管理器实例
            CertificatesManager certificatesManager = CertificatesManager.getInstance();
            // 向证书管理器增加需要自动更新平台证书的商户信息
            certificatesManager.putMerchant(V3WxConfig.merchantId, new WechatPay2Credentials(V3WxConfig.merchantId,
                    new PrivateKeySigner(serialNo, merchantPrivateKey)), V3WxConfig.apiV3key.getBytes(StandardCharsets.UTF_8));
            // 从证书管理器中获取verifier
            //版本>=0.4.0可使用 CertificatesManager.getVerifier(mchId) 得到的验签器替代默认的验签器。
            // 它会定时下载和更新商户对应的微信支付平台证书 （默认下载间隔为UPDATE_INTERVAL_MINUTE）。
            verifier = certificatesManager.getVerifier(V3WxConfig.merchantId);

            //创建一个httpClient
            httpClient = WechatPayHttpClientBuilder.create()
                    .withMerchant(V3WxConfig.merchantId, serialNo, merchantPrivateKey)
                    .withValidator(new WechatPay2Validator(verifier)).build();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("加载秘钥文件失败");
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            log.error("获取平台证书失败");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭 HttpClient
     * @throws IOException
     */
    public static void closeWXClient() throws IOException {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建微信支付订单
     * @param openId 用户唯一标识
     * @param orderId 订单id
     * @param amount 支付价格(单位分)
     * @param description 订单说明
     * @return
     * @throws Exception
     */
    public static String creatOrderJSAPI(String openId,String orderId,Integer amount,String description,String x) throws Exception {
        try {
            initWXPayClient();
            HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi");
            httpPost.addHeader("Accept", "application/json");
            httpPost.addHeader("Content-type", "application/json; charset=utf-8");

            String reqdata = "{"

                    + "\"amount\": {"
                    + "\"total\": "+amount+","
                    + "\"currency\": \"CNY\""
                    + "},"
                    + "\"mchid\": \""+ V3WxConfig.merchantId+"\","
                    + "\"description\": \""+description+"\","
                    + "\"attach\": \""+x+"\","
                    + "\"notify_url\": \""+ V3WxConfig.callBackURL+"\","
                    + "\"payer\": {"
                    + "\"openid\": \""+openId+"\"" + "},"
                    + "\"out_trade_no\": \""+orderId+"\","
                    + "\"goods_tag\": \"WXG\","
                    + "\"appid\": \""+ V3WxConfig.appid+"\""
                    + "}";

            httpPost.setEntity(new StringEntity(reqdata, "utf-8"));
            CloseableHttpResponse response = httpClient.execute(httpPost);
            String bodyAsString = EntityUtils.toString(response.getEntity());
            return bodyAsString;
        }catch (Exception e){
            e.printStackTrace();
            log.error("创建微信支付订单失败");
        }finally {
            closeWXClient();
        }
        return "";
    }

    public static String queryWX(String out_trade_no,String mchid) throws Exception {
        try {
            initWXPayClient();
            HttpGet httpGet = new HttpGet("https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/"+out_trade_no+"?mchid="+mchid);
            httpGet.addHeader("Accept", "application/json");
            httpGet.addHeader("Content-type", "application/json; charset=utf-8");

//            String reqdata = "{"
//
//                    + "\"amount\": {"
//                    + "\"total\": "+amount+","
//                    + "\"currency\": \"CNY\""
//                    + "},"
//                    + "\"mchid\": \""+ V3WxConfig.merchantId+"\","
//                    + "\"description\": \""+description+"\","
//                    + "\"attach\": \""+x+"\","
//                    + "\"notify_url\": \""+ V3WxConfig.callBackURL+"\","
//                    + "\"payer\": {"
//                    + "\"openid\": \""+openId+"\"" + "},"
//                    + "\"out_trade_no\": \""+orderId+"\","
//                    + "\"goods_tag\": \"WXG\","
//                    + "\"appid\": \""+ V3WxConfig.appid+"\""
//                    + "}";
//
//            httpPost.setEntity(new StringEntity(reqdata, "utf-8"));
            CloseableHttpResponse response = httpClient.execute(httpGet);
            String bodyAsString = EntityUtils.toString(response.getEntity());
            return bodyAsString;
        }catch (Exception e){
            e.printStackTrace();
            log.error("创建微信查询订单失败");
        }finally {
            closeWXClient();
        }
        return "";
    }



}
