# java商城crmeb

#### Description
crmeb源代码 java版本单独

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

后端：
crmeb-admin/src/main/java/com/zbkj/admin/job/TaskDemo.java  定时任务  分红和计算点位  都是一天 最后时刻进行结算

user表增加 等级层数
@ApiModelProperty(value = "等级层数loc")
private String spreadLoc;

/**  user  控制层 增加调用方法
* 分页显示用户点位计算
*/
@PreAuthorize("hasAuthority('admin:user:update')")
@ApiOperation(value = "新增点位奖到用户列表")
@RequestMapping(value = "/updatedianwei", method = RequestMethod.POST)
public CommonResult<String> updatedianwei(@RequestParam Integer id, @RequestParam(name = "type") Integer type ) {
if (id!=null){
userService.getDianweiJiang(id,type);
return CommonResult.success();
}
return CommonResult.failed();
}

userDao中增加查询  这个点位下面每日新增 数量
    /********gjp20230802 查询这个点位下面今天新增多少个分销商*********/
    @Select("SELECT count(1) " +
            "FROM   eb_user e  " +
            " WHERE " +
            " e.spread_loc like #{locx} AND e.spread_loc !=#{loc} " +
            " AND   CHAR_LENGTH(e.spread_loc) - CHAR_LENGTH(REPLACE(e.spread_loc, '/','')) <=10 " +
            "  AND promoter_time > #{day1} " +
            "AND promoter_time < #{day2}   " +
            "AND integral > 0 ")
    Integer getUserCountByloc(@Param("locx") String locx, @Param("loc")String loc,@Param("day1") String day1 ,@Param("day2") String day2);


UserServiceImpl  中增加点位奖  佣金表更新  用户余额更新
 根据分组 group 的id   1为普通会员  2为代理商   3为 总代理商

@Override
public void getDianweiJiang(Integer spreadUid, Integer type) {}


前端：
下级需要扫描  推广中的  推广名片来 绑定关系


/****gjp  分红今天新增的  会员 不享受***/
@Override
public List<User> getFenHongUserList() {
QueryWrapper<User> wrapper = Wrappers.query();
String todaybegin = cn.hutool.core.date.DateUtil.date().toString("yyyy-MM-dd 00:00:00");
String todayend = cn.hutool.core.date.DateUtil.date().toString("yyyy-MM-dd 23:59:59");
wrapper.eq("is_promoter", "1");
wrapper.gt("integral",0);
// 小于今天的 开始时间 ，今天新增的会员不分红， 分红是晚上11.50开始执行。
wrapper.lt("promoter_time",todaybegin);
wrapper.eq("mark","1").or().eq("mark","5").or().eq("mark","10");
return userDao.selectList(wrapper);

//  提现加上  手续费和最终金额

       BigDecimal yy= money.subtract(request.getExtractPrice());
        BigDecimal yy2=userExtract.getExtractPrice();
        BigDecimal yy4=request.getExtractPrice();
        BigDecimal yy3= userExtract.getBalance();

        BigDecimal  balancett=yy4;
        BigDecimal  balan3=new BigDecimal(0.03);
        BigDecimal  balan97=new BigDecimal(0.97);
        BigDecimal  balanceshouxufei=balancett.multiply(balan3);
        BigDecimal  balancelast=balancett.multiply(balan97);
        userExtract.setShouxufei(balanceshouxufei);//shouxufei 手续费
        userExtract.setLastprice(balancelast); //zuihou 最后金额

        userExtract.setPaytime(new Date());
        userExtract.setPayuser("usergjp");


// 佣金  最后金额
BigDecimal  yuanlaiprie=user.getBrokeragePrice();
BigDecimal thattimeprice=yuanlaiprie.add(brokerage);

brokerageRecord.setBalance(thattimeprice);
brokerageRecord.setMark(StrUtil.format("获得推广佣金，分佣{},最后金额{}", brokerage,thattimeprice));


// 查询sql 语句 查询下面几层的人数
SELECT count(1)
FROM   eb_user e  
WHERE
e.spread_loc like '1/29135/29138/29136%' AND e.spread_loc !='1/29135/29138/29136' AND
CHAR_LENGTH(e.spread_loc) - CHAR_LENGTH(REPLACE(e.spread_loc, '/','')) <= 10
              AND promoter_time > '2023-09-10 00:00:00' 
            AND promoter_time < '2023-09-10 23:56:00'
            AND integral > 0 

	  public static final String callBackURL="http://www.xt08.top:70010/api/admin/payment/callback/wechat";
