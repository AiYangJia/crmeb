package com.zbkj.front.controller;


import cn.hutool.core.util.StrUtil;
import com.zbkj.common.constants.BrokerageRecordConstants;
import com.zbkj.common.model.user.UserBrokerageRecord;
import com.zbkj.common.page.CommonPage;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.system.SystemUserLevel;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserExperienceRecord;
import com.zbkj.common.model.user.UserIntegralRecord;
import com.zbkj.common.request.*;
import com.zbkj.common.response.*;
import com.zbkj.common.utils.DateUtil;
import com.zbkj.common.vo.FileResultVo;
import com.zbkj.front.service.UserCenterService;
import com.zbkj.service.service.SystemGroupDataService;
import com.zbkj.service.service.UploadService;
import com.zbkj.service.service.UserBrokerageRecordService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 -- 用户中心
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */
@Slf4j
@RestController("FrontUserController")
@RequestMapping("api/front")
@Api(tags = "用户 -- 用户中心")
public class UserController {


    @Autowired
    private UploadService uploadService;



    @Autowired
    private UserService userService;

    @Autowired
    private SystemGroupDataService systemGroupDataService;

    @Autowired
    private UserCenterService userCenterService;


    @ApiOperation(value = "图片上传")
    @RequestMapping(value = "/image", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "模块 用户user,商品product,微信wechat,news文章"),
            @ApiImplicitParam(name = "pid", value = "分类ID 0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片,5文章图片,6组合数据图,7前台用户,8微信系列 ", allowableValues = "range[0,1,2,3,4,5,6,7,8]")
    })
    public CommonResult<FileResultVo> image(MultipartFile multipart,
                                            @RequestParam(value = "model") String model,
                                            @RequestParam(value = "pid") Integer pid) throws IOException {
        return CommonResult.success(uploadService.imageUpload(multipart, model, pid));
    }

    /**
     * 修改密码
     */
    @ApiOperation(value = "手机号修改密码")
    @RequestMapping(value = "/register/reset", method = RequestMethod.POST)
    public CommonResult<Boolean> password(@RequestBody @Validated PasswordRequest request) {
        return CommonResult.success(userService.password(request));
    }

    /**
     * 修改个人资料
     */
    @ApiOperation(value = "修改个人资料")
    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    public CommonResult<Object> personInfo(@RequestBody @Validated UserEditRequest request) {
        if (userService.editUser(request)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    /**
     * 个人中心-用户信息
     */
    @ApiOperation(value = "个人中心-用户信息")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public CommonResult<UserCenterResponse> getUserCenter() {
        return CommonResult.success(userService.getUserCenter());
    }

    /**
     * 换绑手机号校验
     */
    @ApiOperation(value = "换绑手机号校验")
    @RequestMapping(value = "update/binding/verify", method = RequestMethod.POST)
    public CommonResult<Boolean> updatePhoneVerify(@RequestBody @Validated UserBindingPhoneUpdateRequest request) {
        return CommonResult.success(userService.updatePhoneVerify(request));
    }

    /**
     * 绑定手机号
     */
    @ApiOperation(value = "换绑手机号")
    @RequestMapping(value = "update/binding", method = RequestMethod.POST)
    public CommonResult<Boolean> updatePhone(@RequestBody @Validated UserBindingPhoneUpdateRequest request) {
        return CommonResult.success(userService.updatePhone(request));
    }

    /**
     * 用户中心菜单
     */
    @ApiOperation(value = "获取个人中心菜单")
    @RequestMapping(value = "/menu/user", method = RequestMethod.GET)
    public CommonResult<HashMap<String, Object>> getMenuUser() {
        return CommonResult.success(systemGroupDataService.getMenuUser());
    }

    /**
     * 推广数据接口(昨天的佣金 累计提现金额 当前佣金)
     */
    @ApiOperation(value = "推广数据接口(昨天的佣金 累计提现金额 当前佣金)")
    @RequestMapping(value = "/commission", method = RequestMethod.GET)
    public CommonResult<UserCommissionResponse> getCommission() {
        return CommonResult.success(userCenterService.getCommission());
    }

    /**
     * 推广佣金明细
     */
    @ApiOperation(value = "推广佣金明细")
    @RequestMapping(value = "/spread/commission/detail", method = RequestMethod.GET)
    public CommonResult<CommonPage<SpreadCommissionDetailResponse>> getSpreadCommissionDetail(@Validated PageParamRequest pageParamRequest) {
        PageInfo<SpreadCommissionDetailResponse> commissionDetail = userCenterService.getSpreadCommissionDetail(pageParamRequest);
        return CommonResult.success(CommonPage.restPage(commissionDetail));
    }

    /**
     * 推广佣金/提现总和
     */
    @ApiOperation(value = "推广佣金/提现总和")
    @RequestMapping(value = "/spread/count/{type}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "类型 佣金类型3=佣金,4=提现", allowableValues = "range[3,4]", dataType = "int")
    public CommonResult<Map<String, BigDecimal>> getSpreadCountByType(@PathVariable Integer type) {
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("count", userCenterService.getSpreadCountByType(type));
        return CommonResult.success(map);
    }

    /**
     * 提现申请
     */
    @ApiOperation(value = "提现申请")
    @RequestMapping(value = "/extract/cash", method = RequestMethod.POST)
    public CommonResult<Boolean> extractCash(@RequestBody @Validated UserExtractRequest request) {
        return CommonResult.success(userCenterService.extractCash(request));
    }

    /**
     * 提现记录
     */
    @ApiOperation(value = "提现记录")
    @RequestMapping(value = "/extract/record", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserExtractRecordResponse>> getExtractRecord(@Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(CommonPage.restPage(userCenterService.getExtractRecord(pageParamRequest)));
    }

    /**
     * 提现用户信息
     */
    @ApiOperation(value = "提现用户信息")
    @RequestMapping(value = "/extract/user", method = RequestMethod.GET)
    public CommonResult<UserExtractCashResponse> getExtractUser() {
        return CommonResult.success(userCenterService.getExtractUser());
    }

    /**
     * 提现银行
     */
    @ApiOperation(value = "提现银行/提现最低金额")
    @RequestMapping(value = "/extract/bank", method = RequestMethod.GET)
    public CommonResult<List<String>> getExtractBank() {
        return CommonResult.success(userCenterService.getExtractBank());
    }

    /**
     * 会员等级列表
     */
    @ApiOperation(value = "会员等级列表")
    @RequestMapping(value = "/user/level/grade", method = RequestMethod.GET)
    public CommonResult<List<SystemUserLevel>> getUserLevelList() {
        return CommonResult.success(userCenterService.getUserLevelList());
    }

    /**
     * 推广人统计
     */
    @ApiOperation(value = "推广人统计")
    @RequestMapping(value = "/spread/people/count", method = RequestMethod.GET)
    public CommonResult<UserSpreadPeopleResponse>  getSpreadPeopleCount() {
        return CommonResult.success(userCenterService.getSpreadPeopleCount());
    }

    /**
     * 推广人列表
     */
    @ApiOperation(value = "推广人列表")
    @RequestMapping(value = "/spread/people", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserSpreadPeopleItemResponse>> getSpreadPeopleList(@Validated UserSpreadPeopleRequest request, @Validated PageParamRequest pageParamRequest) {
        List<UserSpreadPeopleItemResponse> spreadPeopleList = userCenterService.getSpreadPeopleList(request, pageParamRequest);
        CommonPage<UserSpreadPeopleItemResponse> commonPage = CommonPage.restPage(spreadPeopleList);
        return CommonResult.success(commonPage);
    }

    /**
     * 用户积分信息
     */
    @ApiOperation(value = "用户积分信息")
    @RequestMapping(value = "/integral/user", method = RequestMethod.GET)
    public CommonResult<IntegralUserResponse> getIntegralUser() {
        return CommonResult.success(userCenterService.getIntegralUser());
    }

    /**
     * 积分记录
     */
    @ApiOperation(value = "积分记录")
    @RequestMapping(value = "/integral/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserIntegralRecord>> getIntegralList(@Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(CommonPage.restPage(userCenterService.getUserIntegralRecordList(pageParamRequest)));
    }

    /**
     * 经验记录
     */
    @ApiOperation(value = "经验记录")
    @RequestMapping(value = "/user/expList", method = RequestMethod.GET)
    public CommonResult<CommonPage<UserExperienceRecord>> getExperienceList(@Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(CommonPage.restPage(userCenterService.getUserExperienceList(pageParamRequest)));
    }

    /**
     * 用户资金统计
     */
    @ApiOperation(value = "用户资金统计")
    @RequestMapping(value = "/user/balance", method = RequestMethod.GET)
    public CommonResult<UserBalanceResponse> getUserBalance() {
        return CommonResult.success(userCenterService.getUserBalance());
    }

    /**
     * 推广订单
     */
    @ApiOperation(value = "推广订单")
    @RequestMapping(value = "/spread/order", method = RequestMethod.GET)
    public CommonResult<UserSpreadOrderResponse> getSpreadOrder(@Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(userCenterService.getSpreadOrder(pageParamRequest));
    }

    /**
     * 推广人排行
     * @return List<User>
     */
    @ApiOperation(value = "推广人排行")
    @RequestMapping(value = "/rank", method = RequestMethod.GET)
    public CommonResult<List<User>> getTopSpreadPeopleListByDate(@RequestParam(required = false) String type, @Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(userCenterService.getTopSpreadPeopleListByDate(type, pageParamRequest));
    }

    /**
     * 佣金排行
     * @return 优惠券集合
     */
    @ApiOperation(value = "佣金排行")
    @RequestMapping(value = "/brokerage_rank", method = RequestMethod.GET)
    public CommonResult<List<User>> getTopBrokerageListByDate(@RequestParam String type, @Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(userCenterService.getTopBrokerageListByDate(type, pageParamRequest));
    }

    /**
     * 当前用户在佣金排行第几名
     */
    @ApiOperation(value = "当前用户在佣金排行第几名")
    @RequestMapping(value = "/user/brokerageRankNumber", method = RequestMethod.GET)
    public CommonResult<Integer> getNumberByTop(@RequestParam String type) {
        return CommonResult.success(userCenterService.getNumberByTop(type));
    }

    /**
     * 海报背景图
     */
    @ApiOperation(value = "推广海报图")
    @RequestMapping(value = "/user/spread/banner", method = RequestMethod.GET)
    public CommonResult<List<UserSpreadBannerResponse>>  getSpreadBannerList() {
        return CommonResult.success(userCenterService.getSpreadBannerList());
    }

    /**
     * 绑定推广关系（登录状态）
     * @param spreadPid 推广id
     * @return 绑定结果
     */
    @ApiOperation(value = "绑定推广关系（登录状态）")
    @RequestMapping(value = "/user/bindSpread", method = RequestMethod.GET)
    public CommonResult<Boolean> bindsSpread(Integer spreadPid) {
        userService.bindSpread(spreadPid);
        return CommonResult.success();
    }


    @Autowired
    private UserBrokerageRecordService userBrokerageRecordService;
    /**
     * 互转佣金
     * @param
     * @return 绑定结果
     */
    @ApiOperation(value = "互转佣金")
    @RequestMapping(value = "/user/huzhuanYongjin", method = RequestMethod.POST)
    public CommonResult<String> huzhuanYongjin(@RequestBody @Validated Map<String,Object> map) {
        //根据 用户手机号c查询到 他的账户佣金  可以查看文章代码
        String benname = map.get("benname").toString();
        String sqphone = map.get("sqphone").toString();
        String zhuanchuprice = map.get("sqgood").toString();
        String sqaddress = map.get("sqaddress").toString();
        // 减去现在 要转出的 佣金
        // String benname="15131314835";
        // String sqphone="18531912223";
        User user3 = userService.getByPhone("15131314835");  //
        User user4 = userService.getByPhone("18531912223");  //sqphone
        // 更新2个用户的佣金
        User user1 = userService.getByPhone(benname);  //
        User user2 = userService.getByPhone(sqphone);  //sqphone
        if (zhuanchuprice == null) {
            zhuanchuprice = "0";
            return CommonResult.failed();
        }
        if (user2 != null) {
            BigDecimal user1bro = user1.getBrokeragePrice();

            BigDecimal user2bro = user2.getBrokeragePrice();

            //sqgood


            BigDecimal zhuanchu = new BigDecimal(zhuanchuprice);

            if (zhuanchu.compareTo(user1bro) == 1) {

                return CommonResult.failed();
            }else {

                BigDecimal user1end = user1bro.subtract(zhuanchu);
                BigDecimal user2end = user2bro.add(zhuanchu);
                user1.setBrokeragePrice(user1end);
                user2.setBrokeragePrice(user2end);
                userService.saveOrUpdate(user1);
                userService.saveOrUpdate(user2);
                // 添加佣金 转出记录

                UserBrokerageRecord brokerageRecord = new UserBrokerageRecord();
                brokerageRecord.setUid(user1.getUid());
                brokerageRecord.setLinkId("0");
                brokerageRecord.setLinkType("zqtyh");
                brokerageRecord.setType(BrokerageRecordConstants.BROKERAGE_RECORD_TYPE_SUB);
                brokerageRecord.setTitle(benname + "转佣金" + zhuanchu + "元到对方账户" + sqphone);
                brokerageRecord.setPrice(zhuanchu);
                brokerageRecord.setBalance(user1end);
                brokerageRecord.setMark(StrUtil.format(sqaddress + "佣金转另用户，减少{}", zhuanchu));
                brokerageRecord.setStatus(BrokerageRecordConstants.BROKERAGE_RECORD_STATUS_COMPLETE);
                brokerageRecord.setCreateTime(DateUtil.nowDateTime());
                userBrokerageRecordService.save(brokerageRecord);

                UserBrokerageRecord brokerageRecord2 = new UserBrokerageRecord();
                brokerageRecord2.setUid(user2.getUid());
                brokerageRecord2.setLinkId("0");
                brokerageRecord2.setLinkType("zqtyh");
                brokerageRecord2.setType(BrokerageRecordConstants.BROKERAGE_RECORD_TYPE_ADD);
                brokerageRecord2.setTitle(sqphone + "本用户收到佣金" + zhuanchu + "元,来自对方" + benname + "的转账");
                brokerageRecord2.setPrice(zhuanchu);
                brokerageRecord2.setBalance(user2end);
                brokerageRecord2.setMark(StrUtil.format(sqaddress + "佣金转另用户，增加{}", zhuanchu));
                brokerageRecord2.setStatus(BrokerageRecordConstants.BROKERAGE_RECORD_STATUS_COMPLETE);
                brokerageRecord2.setCreateTime(DateUtil.nowDateTime());
                userBrokerageRecordService.save(brokerageRecord2);
            }
        }
        return CommonResult.success();
    }
}



