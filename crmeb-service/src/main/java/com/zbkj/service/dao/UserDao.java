package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.user.User;
import com.zbkj.common.response.UserSpreadPeopleItemResponse;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 用户表 Mapper 接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
public interface UserDao extends BaseMapper<User> {

    List<UserSpreadPeopleItemResponse> getSpreadPeopleList(Map<String, Object> map);

    List<User> findAdminList(Map<String, Object> map);

    @Select("SELECT * " +
            "FROM   eb_user e  " +
            " WHERE " +

            " e.spread_loc like #{locx} AND e.spread_loc !=#{loc} " +
            " AND   CHAR_LENGTH(e.spread_loc) - CHAR_LENGTH(REPLACE(e.spread_loc, '/','')) <=10 " +
            "  AND promoter_time > #{day1} " +
            "AND promoter_time < #{day2}   " +
            "AND integral > 0 ")
    List<User> getUserListByloc(@Param("locx") String locx, @Param("loc")String loc,@Param("day1") String day1 ,@Param("day2") String day2);


    /********gjp20230802 查询这个点位下面今天新增多少个分销商*********/
    @Select("SELECT count(1) " +
            "FROM   eb_user e  " +
            " WHERE " +
            " e.spread_loc like #{locx} AND e.spread_loc !=#{loc} " +
            " AND   CHAR_LENGTH(e.spread_loc) - CHAR_LENGTH(REPLACE(e.spread_loc, '/','')) <= #{knum} " +
            "  AND promoter_time > #{day1} " +
            "AND promoter_time < #{day2}   " +
            "AND integral > 0 ")
    Integer getUserCountByloc(@Param("locx") String locx, @Param("loc")String loc,@Param("knum")String knum,@Param("day1") String day1 ,@Param("day2") String day2);


}
