package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.user.UserBrokerageRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户佣金记录表 Mapper 接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
public interface UserBrokerageRecordDao extends BaseMapper<UserBrokerageRecord> {


    /********gjp20230802 查询这个点位分佣超过 10条 或者 20条 获取的数量去判是否升级*********/
     @Select("SELECT  count(*) as xnum from eb_user_brokerage_record e where  (e.price ='40'or e.price ='200' or e.price ='400')"+
            " and title='获得推广佣金' and uid=#{uid} ")
    Integer getUserCountTen(@Param("uid") Integer uid);



}
